#ifndef WORLD_H
#define WORLD_H

#include <map>
#include <utility>
#include <vector>
#include "robot.h"

typedef std::map<int, Robot> IntegerRobotMap;
typedef std::pair<int, int> Position;
typedef std::vector<Robot> RobotVector;

class World
{
private:
    /* data */
    IntegerRobotMap robotsByNumber;
    RobotVector robots;

public:
    void addRobot(const Robot &robi);
    void removeRobot(const Robot &robi);
    
    Robot getRobotAtPosition(const Position &position);
};
#endif