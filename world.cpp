#include "world.h"

void World::addRobot(const Robot &robi)
{
    robotsByNumber.insert(IntegerRobotMap::value_type(robi.getNumber(), robi));
}

void World::removeRobot(const Robot &robi)
{
    robotsByNumber.erase(robi.getNumber());
}

Robot World::getRobotAtPosition(const Position &position) {
    for (const auto &kv : robotsByNumber)
    {
        Robot robi = kv.second;
        if (robi.getPosition() == position) {
            return robi;
        }
    }
}