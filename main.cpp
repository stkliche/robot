#include <set>
#include <iostream>
#include "robot.h"


int main()
{
    std::multiset<Robot> robots;
    robots.insert(Robot(9,1, 1, 6));
    robots.insert(Robot(2, 1, 1, 5));
    robots.insert(Robot(1, 2, 3, 4));
    robots.insert(Robot(4, 2, 2, 4));

/*  for (auto &robot : robots) {
        robot.show();
    }
    */
    Robot robi(1, 1, 3, 4);
    robi.show();
    robi.move();
    robi.move();
    robi.turnRight();
    robi.move();

    robi.show();
    std::cout << "robi travelled " << robi.travelled();
    return 0;
}