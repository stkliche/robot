#ifndef ROBOT_H
#define ROBOT_H

#include <vector>
#include <utility>

typedef std::pair<int, int> Position;

class Robot
{
private:
    /* data */
    char direction = 'N';
    int stepsMoved = 0;

    int robot_number;
    int team_number;
    Position position;

public:
    // constructor
    Robot(int number, int team, int x, int y);

    // interface methods
    void show() const;
    int travelled();
    void turnLeft();
    void turnRight();

    int getTeam() const { return team_number; };
    int getNumber() const { return robot_number; };
    Position getPosition() const { return position; };

    // shoult only move if there is no player of own team on the field
    void move(); 
    // overload comparator
    bool operator<(const Robot &robi) const { return robot_number < robi.robot_number; };
};
#endif