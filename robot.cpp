#include "robot.h"
#include <iostream>

Robot::Robot(int number, int team, int x, int y)
{
    team_number = team;
    robot_number = number;
    position = std::pair(x, y);
}

void Robot::show() const
{
    std::cout << "Robot " << robot_number
            << " with coordinates x:" << position.first
            << " y:" << position.first << std::endl;
}

void Robot::turnLeft() {
    std::cout << "debug - Direction was " << direction << std::endl;
    switch (direction)
    {
    case 'N':
        direction = 'W';
        break;
    case 'W':
        direction = 'S';
        break;
    case 'S':
        direction = 'E';
        break;
    }

    std::cout << "debug - Direction now is " << direction << std::endl;
}

void Robot::turnRight()
{
    std::cout << "debug - Direction was " << direction << std::endl;
    switch (direction)
    {
    case 'N':
        direction = 'E';
        break;
    case 'E':
        direction = 'S';
        break;
    case 'S':
        direction = 'W';
        break;
    }

    std::cout << "debug - Direction now is " << direction << std::endl;
}
void Robot::move() {
    std::cout << "debug - initial position x,y " << position.first << ", " << position.second << std::endl;
    switch (direction) {
        case 'N':
            position.second++;
            break;
        case 'E':
            position.first++;
            break;
        case 'S':
            position.second--;
            break;
        case 'W':
            position.first--;
            break;
    }

    stepsMoved++;
    std::cout << "debug - new position x,y " << position.first << ", " << position.second << std::endl;
}
int Robot::travelled() {
    return stepsMoved;
}